/*
 * =====================================================================================
 *
 *       Filename:  exp_server.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2014年05月04日 22时45分10秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  www.blueecho.info 
 *   Organization:  
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <linux/in.h>
#include <errno.h>
/*打印错误信息*/
#define ERR_EXIT(msg) \
	do {\
		perror(strerror(msg));\
		return -1;\
	}while(0)
#define PORT 		8888	//侦听端口
#define BACKLOG		20	//侦听队列长度
#define BUFSIZE		1024	//缓冲期大小

void process_conn_server(int s);


int main(int argc, char *argv[]){
	/*ss为服务器socket描述符, sc为客户端socket描述符*/	
	int ss,sc;
	struct sockaddr_in server_addr;	//服务器地址结构
	struct sockaddr_in client_addr;	//客户端地址结构
	int err;
	pid_t pid;
	/*建立一个流式套接字*/
	ss = socket(AF_INET, SOCK_STREAM, 0);
	if (0 > ss){
		ERR_EXIT(errno);
	}
	
	/*设置服务器地址*/
	bzero(&server_addr, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = htonl(INADDR_ANY); 
	server_addr.sin_port = htons(PORT);
	/*绑定地址结构到套接字描述符*/
	err = bind(ss, (struct sockaddr *)&server_addr, sizeof(server_addr));
	if (0 > err){
		ERR_EXIT(errno);
	}
	/*设置侦听*/
	err =  listen(ss, BACKLOG);
	if (0 > err){
		ERR_EXIT(errno);
	}
		
	for (;;){
		int addrlen =  sizeof(struct sockaddr);
		/*接受客户端连接*/
		sc = accept(ss, (struct sockaddr *)&client_addr, &addrlen);
		
		if (0 > sc){
			perror(strerror(errno));
			continue;
		}	
		/*建立新的进程处理连接*/
		pid = fork();
		if (0 == pid){
			close(ss);
			process_conn_server(sc);
		}else{
			close(sc);
		}
		
	}
}

/*服务器对客户端处理*/
void process_conn_server(int s){
	ssize_t size = 0;
	char buffer[BUFSIZE];

	for (;;){
		size = read(s, buffer, sizeof(buffer));
		if (0 == size){
			return ;
		}
		sprintf(buffer, "%d bytes altogether\n",size);
		write(s, buffer, strlen(buffer)+1);
	}
}
