/*
 * =====================================================================================
 *
 *       Filename:  exp_client.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2014年05月05日 22时10分13秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  www.blueecho.info
 *   Organization:  
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <linux/in.h>
#include <errno.h>

/*打印错误信息*/
#define ERR_EXIT(msg)\
	do{\
		perror(strerror(errno));\
		return -1;\
	}while(0)

#define PORT 8888	//侦听端口
#define BUFSIZE 1024	// 缓存大小
void process_conn_client(int s);
int main(int argc, char *argv[]){
	int s;		//socket 描述符
	struct sockaddr_in server_addr;	//服务器地质结构
	int err;	//返回值
	
	s = socket(AF_INET, SOCK_STREAM, 0);	// 简历一个流式socket
	if (0 > s){
		/*出错处理*/
		ERR_EXIT(errno);
	}
	/*设置服务器socket地址*/
	bzero(&server_addr, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = htonl(INADDR_ANY); 
	server_addr.sin_port = htons(PORT);
	/*将用户输入的字符串IP地址转换为整形*/
	inet_pton(AF_INET, argv[1], &server_addr.sin_addr);	
	/*连接服务器*/
	connect(s, (struct sockaddr*)&server_addr, sizeof(server_addr));
	/* 客户端处理*/	
	process_conn_client(s);
	close(s);
	
	return 0;
	
}

/*客户端处理函数*/
void process_conn_client(int s){
	ssize_t size = 0;
	char buffer[BUFSIZE];	/*数据缓冲区*/
	
	for (;;){
		/*从标准输入读取数据*/
		size = read(0, buffer, sizeof(buffer));
		if (0 < size){
			/*发送数据给服务器*/
			write(s, buffer, size);
			/*从服务器获取数据*/
			size = read(s,buffer, sizeof(buffer));
			/*打印数据*/			
			write(1, buffer, size);
		}
	}
}
	
